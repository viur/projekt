from .index import index
from .forum import Forum as forum
from server.modules.page import Page as page
from server.modules.user import CustomUser as user
from .product import Product as product
from .cart import Cart as cart
from .order import Order as order
from server.modules.file import File as file
from news import News as news
from news import News_Category as news_category
from .formmailer import contact

from server.modules.site import Site as site
