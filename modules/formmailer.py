# -*- coding: utf-8 -*-
from server.modules.formmailer import Formmailer
from server.bones import *
from server.skeleton import Skeleton
import re

def validateEmail( value ):
	regex = re.compile("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}")
	res = regex.findall( value.lower() )
	if len( res ) == 1:
		return None
	else:
		return u"Invalid eMailadress"

class contact_mailskel( Skeleton ):
	entityName = "unused"
	id = None
	firstname = stringBone( descr="First Name", params={"searchable": True, "frontend_list_visible": True} , unsharp=True, required=True )
	lastname = stringBone( descr="Last Name", params={"searchable": True, "frontend_list_visible": True} , unsharp=True, required=True )
	phone = stringBone( descr="Phone", params={"searchable": True, "frontend_list_visible": True}, required=False)
#	zipcode = numericBone( descr="PLZ", params={"searchable": True, "frontend_list_visible": True, "dest_box": "allgemein"} )
#	city = stringBone( descr="Stadt", params={"searchable": True, "frontend_list_visible": True, "dest_box": "allgemein"} )
	#city = cityBone.cityBone( descr="Stadt", params={"searchable": True, "frontend_list_visible": True,  "dest_box": "allgemein"}, required=True )
	email= stringBone( descr="email", params={"searchable": True, "frontend_list_visible": True,  "dest_box": "allgemein"},  required=True, vfunc=validateEmail )
	descr = textBone( descr="message",  params={"searchable": True, "frontend_list_visible": True,"dest_box":"message"},  required=True )
#	captcha = captchaBone( descr="", publicKey="", privateKey="" )


	
class contact( Formmailer ): 
	mailSkel = contact_mailskel
	addTemplate = "formmailer_add"
	mailTemplate = "contact"
	sendMailSuccessTemplate = "formmailer_success"
	
	def canUse( self ):
		return( True )
	
	def getRcpts( self,  skel ):
		return ["me@example.com"]

contact.jinja2=True
contact.internalExposed = True
