# -*- coding: utf-8 -*-
from server.render.jinja2 import default as default_render


class index( object ):
	
	def __init__(self, *args, **kwargs ):
		self.render = default_render( self )

	def index( self, *args,  **kwargs ):
		return( self.render.view( {}, tpl="index" ) )
	index.exposed=True
	
index.jinja2=True
