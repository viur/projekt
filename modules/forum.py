# -*- coding: utf-8 -*-
from server.modules.forum import Forum as Forum_orig
from server.skeleton import Skeleton
from server.bones import *
from .product import ProductSkel
from hashlib import md5

class Forum( Forum_orig ):
	def canList(self, id):
		#We list all boards, regardless if the user is allowed to see its content or not
		return( True )

	def jinjaEnv(self, env ):
		env = super( Forum, self ).jinjaEnv( env )
		env.globals["getGravatarImage"] = self.gravatarImage
		return( env )

	def gravatarImage(self, user ):
		if not user:
			return("/static/no_avatar.png")
		return( "http://www.gravatar.com/avatar/%s?s=111" % md5( user["name"].strip() ).hexdigest() )
	
	
