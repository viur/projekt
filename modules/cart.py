# -*- coding: utf-8 -*-
from server.modules.cart import Cart as Cart_orig
from server.skeleton import Skeleton
from server.bones import *
from .product import ProductSkel

class Cart( Cart_orig ):
	productSkel = ProductSkel
