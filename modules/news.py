# -*- coding: utf-8 -*-
from server.bones import *
from server import utils, session, request
from server.applications.list import List
from server.skeleton import Skeleton
#from google.appengine.ext import deferred
from datetime import datetime
#from fws.bones.textBone import HtmlSerializer
#import xhtml2pdf
#import xhtml2pdf.pisa as pisa
#import StringIO


class NewsSkel( Skeleton ):
	kindName = "news"
	searchindex = "news"
	name = stringBone(  descr="Name",indexed=True )
	category = relationalBone( type="news_category", descr="Category",indexed=True, required=True, multiple=True )
	teaser = textBone( descr="Teasertext", plaintext=True, required=True )
	descr = textBone( descr="Content",plaintext=False, indexed=True, required=True )
	image = fileBone( descr="Teaserimage" )
	creationdate = dateBone(  descr="Creationdate",creationmagic=True )
	active = selectOneBone( descr="Visible", values={"1":"Yes","0":"No"}, required=True )

class News( List ): 
	viewSkel = NewsSkel
	addSkel = NewsSkel
	editSkel = NewsSkel
	listTemplate = "news_list"
	viewTemplate = "news_view"
	
	adminInfo = {	"name": u"Newsentries", #Name of this modul, as shown in Apex (will be translated at runtime)
				"handler": "base",  #Which handler to invoke
				"icon": "icons/modules/news.png", #Icon for this modul
				"columns":["name","category","creationdate"],
				"previewurls" : {"Webnews":"/{{modul}}/view/{{id}}"}
				}

#	def pdf(self, *args, **kwargs ):
#		if "id" in kwargs:
#			id = kwargs["id"]
#		elif( len( args ) >= 1 ):
#			id= args[0]
#		currNews = self.viewSkel()
#		currNews.fromDB( id )
#		html = self.render.view( self,  currNews, tpl="news_pdfexport" )
#		result = StringIO.StringIO()
#		pdf = pisa.CreatePDF( StringIO.StringIO(html.encode('ascii', 'xmlcharrefreplace')), result )
#		request.current.get().response.headers['Content-Type'] = "application/pdf"
#		return( result.getvalue() )
#	pdf.exposed=True

	def listFilter( self, filter ):
			return( filter )
News.jinja2 = True

class News_CategorySkel( Skeleton ):
	kindName = "news_category"
	searchIndex = "news_category"
	name = stringBone(  descr="Name", indexed=True,required=True )

class News_Category( List ):
	viewSkel = News_CategorySkel
	addSkel = News_CategorySkel
	editSkel = News_CategorySkel

	adminInfo = {	"name": "News-Categories", #Name of this modul, as shown in Apex (will be translated at runtime)
				"handler": "base",  #Which handler to invoke
				"icon": "icons/modules/categories.png", #Icon for this modul
				"previewurls" : None
				}


News_Category.jinja2 = True
