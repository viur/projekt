# -*- coding: utf-8 -*-
from server.modules.order import Order as Order_orig, PaymentProviderSofort, PaymentProviderPayPal
from server.skeleton import Skeleton
from server.bones import *
from server import utils
from server import db
import logging
from .product import ProductSkel
from google.appengine.ext import ndb

class Order( Order_orig, PaymentProviderSofort, PaymentProviderPayPal ):
	class amountSkel( Skeleton ):
		product = relationalBone( descr="Product", type="product", multiple=True )


	def sendOrderCompleteEmail(self, orderID):
		order = Order.listSkel()
		order.fromDB( orderID )
		res = {}
		res["order"] = order.getValues()
		res["bill"] = self.getBillItems( orderID)
		utils.sendEMail([order.bill_email.value], "order_complete", res )
		
	def sendOrderPayedEmail(self, orderID):
		order = Order.listSkel()
		order.fromDB( orderID )
		res = {}
		res["order"] = order.getValues()
		res["bill"] = self.getBillItems( orderID)
		utils.sendEMail([order.bill_email.value], "order_payed", res )

	def sendOrderShippedEmail(self, orderID):
		order = Order.listSkel()
		order.fromDB( orderID )
		res = {}
		res["order"] = order.getValues()
		res["bill"] = self.getBillItems( orderID)
		utils.sendEMail([order.bill_email.value], "order_shiped", res )


	def sendOrderCanceledEMail(self, orderID):
		order = Order.listSkel()
		order.fromDB( orderID )
		res = {}
		res["order"] = order.getValues()
		res["bill"] = self.getBillItems( orderID)
		utils.sendEMail([order.bill_email.value], "order_canceled", res )

	
	def sendOrderClosedEmail(self, orderID):
		order = Order.listSkel()
		order.fromDB( orderID )
		res = {}
		res["order"] = order.getValues()
		res["bill"] = self.getBillItems( orderID)
		utils.sendEMail([order.bill_email.value], "order_closed", res )

	def getBillItems(self, orderID ):
		"""
		Returns all Items for the given Order.
		Must be overriden.

		@type orderID: string
		@param orderID: ID to mark completed
		@return: [ ( Int Amount, Unicode Description , Float Price of one Item, Float Price of all Items (normaly Price of one Item*Amount), Float Included Tax )  ] 
		"""
		skel = self.amountSkel()
		if not skel.fromDB( orderID ):
			return( [] )
		tmpRes = {}
		shipPrice = 0
		for product in skel.product.value:
			if product["id"] in tmpRes.keys():
				tmpRes[ product["id"] ]["amt"] += 1
			else:
				prod = db.Get( db.Key( product["id"] ) )
				tmpRes[ product["id"] ] = product
				tmpRes[ product["id"] ]["amt"] = 1
				tmpRes[ product["id"] ]["price"] = prod["price"]
				tmpRes[ product["id"] ]["tax"] = prod["tax"]
				if prod["shipprice"] and prod["shipprice"]>shipPrice:
					shipPrice = prod["shipprice"]
		res = []
		for k, v in tmpRes.items():
			res.append( (v["amt"], v["name"], v["price"], v["price"]*v["amt"], v["tax"]) )
		return( res )
	getBillItems.internalExposed = True
