# -*- coding: utf-8 -*-
from server.applications.tree import Tree, TreeSkel
from server.bones import *
from server import db

class ProductSkel( TreeSkel ):
	kindName = "product"
	searchindex = "product"
	name = stringBone( descr="Name", required=True,indexed=True )
	subline = stringBone( descr="Subline", required=True,unsharp=True,indexed=True )
	descr = documentBone( descr=u"Big Description", required=True, indexed=True )
	image = fileBone( descr="Main image", multiple=False,required=False )
	images = fileBone( descr="Additional images", multiple=True,required=False )
	price = numericBone( descr="Price", precision=2, required=True )
	shipprice = numericBone( descr="Price for Shippig", precision=2, required=True )
	tax = selectOneBone( descr=u"Tax", values = {7:u"7%", 19:"19%" } )
	hrk = baseBone( descr="Human readable key", visible=False, required=False, readOnly=True )

	def postProcessSerializedData( self, id,  dbfields ): #Build our human readable key
		try:
			obj = db.Get( db.Key( id ) )
		except db.EntityNotFoundError:
			return
		nr = db.Key( id ).id_or_name()
		hrk = "!%s-%s" % ( str( "".join( [ x for x in obj["name"].lower() if x in "0123456789 abcdefghijklmnopqrstuvwxyz"] ) ).replace(" ", "_"), nr )
		obj["hrk"] = hrk
		db.Put( obj )

class Product( Tree ):
	viewSkel = ProductSkel
	editSkel = ProductSkel
	addSkel = ProductSkel
	
	listRootNodeContentsTemplate="product_list"
	viewTemplate = "product_view"
	
	adminInfo = {	"name": u"Products", #Name of this modul, as shown in Apex (will be translated at runtime)
			"handler": "tree",  #Which handler to invoke
			"icon": "icons/modules/produktdatenbank.png", #Icon for this modul
			"columns":["name","category","creationdate"],
			"previewurls" : {"Webnews":"/{{modul}}/view/{{id}}"}
			}


	def index(self, *args, **kwargs): #Provide a nice entry-point
		return( self.list( self.getAvailableRootNodes()[0]["key"], "/" ) )
	index.exposed=True

	def getAvailableRootNodes( self, name=None ): #We use one application-global RootNode (not different Shops per user :) )
		repo = self.ensureOwnModulRootNode()
		res = [ { "name":_("Shop"), "key": str(repo.key()) } ]
		return( res )

	def canList( self, folder, repository, path ):
		return( True ) #The shop is not restricted
		
	def canView(self, id):
		return( True ) #The shop is not restricted
