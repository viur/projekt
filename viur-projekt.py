#!/usr/bin/python
# -*- coding: utf-8 -*-
import modules
import locale
import server
from server.config import conf

application = server.setup( modules )


#conf["sofort"] = {	"userid":"xx",
#					"projectid":"xx",
#					"projectpassword":"xxxx*",
#					"notificationpassword":"xxxxx" }

#conf["paypal"] = {'USER' : 'xxxx', 
#				'PWD' : 'xxxxx', 
#				'SIGNATURE' : 'xxxxx'
#				}

conf["viur.forceSSL"] = True

def main():
	server.run()

if __name__ == '__main__':
	main()
