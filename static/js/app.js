/* 
* Skeleton V1.0.2
* Copyright 2011, Dave Gamache
* www.getskeleton.com
* Free to use under the MIT license.
* http://www.opensource.org/licenses/mit-license.php
* 5/20/2011
*/	

	function scrollToPlace(place) {
	$('html,body').animate(
		{
			scrollTop: place.offset().top -60
		},
		{
			duration: 1000
		}
	);
}


function scrolleventhandler () {
                thescroll=$(window).scrollTop();
                if (thescroll<=0) {
                    thescroll=0;
                }

								$('#navcontainer a').removeClass('active');
								$("#navcontainer a").each(function (index) {
									navitemname=$(this).attr("href");
									navitemoffset=$(navitemname).offset().top
									//alert (navitemname+":  "+navitemoffset+" ? thescroll: "+thescroll);
									if (thescroll<navitemoffset) {
												$('#navcontainer a[href='+navitemname+'] ').addClass('active');
												//alert (navitemname);
												return false;
									}
									
								})

								
                if (thescroll>=($('#closingstatement').offset().top-800)) {
                    if ($('#navcontainer').css('display')=='none') {
										} else {
                        $('#navcontainer').fadeOut("slow");
                    }
                } else {
									  if ($('#navcontainer').css('display')=='none') {
											$('#navcontainer').fadeIn("slow");
										} 
								}
}

$(document).ready(function() {


	/* Fade in
	================================================== */

	$(document).ready(function () {
		$('.container').hide().delay(0).fadeIn(1000);
	});


	
	/* Scroll To Top
	================================================== */
	
	
	$(document).ready(function() {
   
 		$('a[href=#top]').click(function(){
        	$('html, body').animate({scrollTop:0}, 'slow');
        	return false;
		});

	$("#navcontainer a").live('click tap', function() {
		var destination = $(this).attr("href");
		scrollToPlace($(destination))
		return false;
	})

	$(window).scroll(scrolleventhandler);

	});


	/* Fade Label
	================================================== */

	$(document).ready(function(){
		$("label").inFieldLabels();
	});

	

});